'use strict';
function addUserFetch(userInfo) {
  // напишите POST-запрос используя метод fetch
  let options = {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(userInfo)
  };

  let promise = fetch(`/users`, options)

  // я не понимаю разницу между return и resolve

  // тут вроде после второго. Зачем нужен resolve? 
  return promise
    .then(response => {
      if (response.ok) {
        return response.json()
      }
      else {
        throw new Error(`Запрос завершился не успешно ${response.status} ${response.statusText}`)
      }
    })
    .then(userInfoJson => {
      return userInfoJson.id
    })
    .catch(error => {
      console.log(error)
    })

  // В этом случае возращается промис после первого зена
  // promise
  //   .then(response => {
  //     return response.json()
  //   })
  //   .then(userInfoJson => {
  //     return userInfoJson.id
  //   })
  // return promise

}

function getUserFetch(id) {
  // напишите GET-запрос используя метод fetch
  let promise = fetch(`/users/${id}`)

  return promise
    .then(response => {
      if (response.ok) {
        return response.json()
      }
      else {
        throw new Error(`Запрос завершился не успешно ${response.status} ${response.statusText}`)
      }
    })
    .then(userData => {
      return userData
    })
    .catch((error) => {
      console.log(error);
    })
}

function addUserXHR(userInfo) {
  // напишите POST-запрос используя XMLHttpRequest
  let promise = new Promise((resolve, reject) => {
    if (userInfo.hasOwnProperty(`id`)) {
      reject(new Error(`неверные входные данные`))
    }
    const xhr = new XMLHttpRequest();
    xhr.open('POST', `/users`, false);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify(userInfo));
    if (xhr.status >= 200 && xhr.status <= 299) {
      let addedUserObject = JSON.parse(xhr.responseText);
      resolve(addedUserObject.id);
    } else {
      reject(new Error(`${xhr.status} ${xhr.statusText}`))
    }
  })
  return promise
}

function getUserXHR(id) {
  // напишите GET-запрос используя XMLHttpRequest
  let promise = new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.open('GET', `/users/${id}`, false);
    xhr.send();
    if (xhr.status !== 200) {
      reject(new Error(`${xhr.status} ${xhr.statusText}`))
    } else {
      resolve(JSON.parse(xhr.responseText));
    }
  })
  return promise
}


function checkWorkFetch() {
  addUserFetch({ name: "Alice", lastname: "FetchAPI" })
    .then(userId => {
      console.log(`Был добавлен пользователь с userId ${userId}`);
      return getUserFetch(userId);
    })
    .then(userInfo => {
      console.log(`Был получен пользователь:`, userInfo);
    })
    .catch(error => {
      console.error(error);
    });
}
function checkWorkXHR() {
  addUserXHR({ name: "Bob", lastname: "XHR" })
    .then(userId => {
      console.log(`Был добавлен пользователь с userId ${userId}`);
      return getUserFetch(userId);
    })
    .then(userInfo => {
      console.log(`Был получен пользователь:`, userInfo);
    })
    .catch(error => {
      console.error(error);
    });
}

// checkWorkFetch();
checkWorkXHR()